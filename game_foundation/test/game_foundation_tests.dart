import 'package:unittest/unittest.dart';
import 'package:unittest/html_enhanced_config.dart';
import 'package:game_foundation/game_foundation.dart';

void expectedTwoPointPath(JumpPointSearch jps, Node startNode, endNode) {
  jps.grid.setStartNode(startNode.row, startNode.col); jps.grid.setEndNode(endNode.row, endNode.col);
  var actual = jps.path;
  var expected = [startNode, endNode];
  expect(actual, equals(expected));
}

void expectedPath(JumpPointSearch jps, List<Node> nodes) {
  Node startNode = nodes.first, endNode = nodes.last;
  jps.grid.setStartNode(startNode.row, startNode.col); jps.grid.setEndNode(endNode.row, endNode.col);
  var actual = jps.path;
  expect(actual, equals(nodes));
}

void main() {
  useHtmlEnhancedConfiguration();
  group('A group of tests around a particular behavior', () {
    test('Simple test', () {
      var actual = 1 + 2;
      var expected = 3;
      expect(actual, equals(expected));
    });
  });

  group('Tests for a Grid object', () {
    Grid grid = new Grid.withMap([
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    ]);
    test('Find all of the nodes that surround a point with free space all around', () {
      var actual = grid.adjacentNodes(grid.getNode(1, 1));
      var expected = [
          grid.getNode(0, 0), grid.getNode(0, 1), grid.getNode(0, 2),
          grid.getNode(1, 0), grid.getNode(1, 2),
          grid.getNode(2, 0), grid.getNode(2, 1), grid.getNode(2, 2)
      ];
      expect(actual, equals(expected));
    });
    test('Find all of the nodes that surround a point in the top row', () {
      var actual = grid.adjacentNodes(grid.getNode(0, 1));
      var expected = [
          grid.getNode(0, 0), grid.getNode(0, 2),
          grid.getNode(1, 0), grid.getNode(1, 1), grid.getNode(1, 2)
      ];
      expect(actual, equals(expected));

    });
    test('Find all of the nodes that surround a point in the upper left hand corner', () {
      var actual = grid.adjacentNodes(grid.getNode(0, 0));
      var expected = [
          grid.getNode(0, 1),
          grid.getNode(1, 0), grid.getNode(1, 1),
      ];
      expect(actual, equals(expected));

    });
    test('Find all of the nodes that surround a point in the bottom row', () {
      var bottom = grid.rows - 1;
      var actual = grid.adjacentNodes(grid.getNode(bottom, 1));
      var expected = [
          grid.getNode(bottom - 1, 0), grid.getNode(bottom - 1, 1), grid.getNode(bottom - 1, 2),
          grid.getNode(bottom, 0), grid.getNode(bottom, 2)
      ];
      expect(actual, equals(expected));

    });
  });

  group('Direction tests', () {
    test('Testing for north east', () {
      var actual = new Direction(-1, 1);
      expect(actual, equals(const Direction.northEast()));
    });
    test('Testing for north west', () {
      var actual = new Direction(-1, -1);
      expect(actual, equals(const Direction.northWest()));
    });
    test('Testing for south east', () {
      var actual = new Direction(1, 1);
      expect(actual, equals(const Direction.southEast()));
    });
    test('Testing for south west', () {
      var actual = new Direction(1, -1);
      expect(actual, equals(const Direction.southWest()));
    });
  });

  group('Jump Point Search Directional Test', (){
    Grid grid = new Grid.withMap([
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    ]);
    JumpPointSearch jps = new JumpPointSearch(grid, CostHeuristics.manhattan);

    test('Directions NE for a jump point can be determined, ordered from most to least promising', () {
      grid.setStartNode(5, 1); grid.setEndNode(0, 10);
      var actual = jps.directions(grid.startNode);
      expect(actual.length, equals(4));
      expect(actual.first, equals(const Direction.northEast()));
      expect(actual.last, equals(const Direction.southWest()));
    });

    test('Directions SE for a jump point can be determined, ordered from most to least promising', () {
      grid.setStartNode(5, 1); grid.setEndNode(6, 10);
      var actual = jps.directions(grid.startNode);
      expect(actual.length, equals(4));
      expect(actual.first, equals(const Direction.southEast()));
      expect(actual.last, equals(const Direction.northWest()));
    });

  });

  group('Tests for Jump Point Search', () {
    Grid grid = new Grid.withMap([
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    ]);
    JumpPointSearch jps = new JumpPointSearch(grid, CostHeuristics.manhattan);

    test('JPS E with no obstacles results in a path with two nodes ', () {
      expectedPath(jps, [grid.getNode(3, 3), grid.getNode(3, 9)]);
    });

    test('JPS W with no obstacles results in a path with two nodes ', () {
      expectedPath(jps, [grid.getNode(3, 9), grid.getNode(3, 3)]);
    });

    test('JPS N with no obstacles results in a path with two nodes ', () {
      expectedPath(jps, [grid.getNode(5, 3), grid.getNode(0, 3)]);
    });

    test('JPS S with no obstacles results in a path with two nodes ', () {
      expectedPath(jps, [grid.getNode(0, 3), grid.getNode(5, 3)]);
    });

    test('JPS SE with no obstacles results in a path with two nodes ', () {
      expectedPath(jps, [grid.getNode(0, 0), grid.getNode(6, 6)]);
    });

    test('JPS NE with no obstacles results in a path with two nodes ', () {
      expectedPath(jps, [grid.getNode(6, 0), grid.getNode(0, 6)]);
    });

    test('JPS NW with no obstacles results in a path with two nodes ', () {
      expectedPath(jps, [grid.getNode(6, 10), grid.getNode(0, 4)]);
    });

    test('JPS SW with no obstacles results in a path with two nodes ', () {
      expectedPath(jps, [grid.getNode(0, 10), grid.getNode(6, 4)]);
    });

    test('JPS NE with no obstacles and slightly more complicated start/end results in a path with three nodes ', () {
      expectedPath(jps, [grid.getNode(5, 1), grid.getNode(0, 6), grid.getNode(0, 10)]);
    });

    test('JPS SE with no obstacles and slightly more complicated start/end results in a path with three nodes ', () {
      expectedPath(jps, [grid.getNode(5, 1), grid.getNode(6, 2), grid.getNode(6, 10)]);
    });

  });

}            