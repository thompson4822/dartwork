part of game_foundation;


/**
 * A* algorithm for determining shortest path between a source and destination on a grid
 */
class AStar {
  List<Node> open, closed, path;
  Grid grid;
  Node startNode, endNode;
  CostHeuristic heuristic;

  /// Create the class with the given heuristic for finding the path
  AStar({CostHeuristic this.heuristic: CostHeuristics.diagonal});

  void calculateCost(Node node) {

  }

  /// What are the possible nodes from this node, with the cost calculated and ordered from least to most expensive?
  List<Node> possibleNodes(Node start) {
    List<Node> possibilities = [];
    if(start.row > 0) {
/*
      possibilities.add(grid.getNode(row, col - 1));
      if(start.col > 0) {
        possibilities.add(grid.getNode(row - 1, col - 1));
      }
      if(start.col < grid.columns)
        possibilities.add(grid.getNode(row - 1, col + 1));
*/
    }
  }

  List<Node> solution() {
    /*
    Algorithm:
    Add the starting node to the open list
    Repeat:
      1) Look for the lowest f cost square on the open list. This is called the current square
      2) Move it to the closed list
      3) For all of the adjacent squares:
        - If it is not walkable or it is on the closed list, ignore it
        - If it isn't on the open list, add it to the open list
     */
  }
}
