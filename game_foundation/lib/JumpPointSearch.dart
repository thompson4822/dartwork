
part of game_foundation;

class Direction {
  final int dx, dy;
  Direction(int this.dy, this.dx);

  bool operator == (Direction other) => dx == other.dx && dy == other.dy;

  const Direction.northEast() : this(-1, 1);
  const Direction.northWest() : this(-1, -1);
  const Direction.southEast() : this(1, 1);
  const Direction.southWest() : this(1, -1);

  @override String toString() => "($dy, $dx)";
}

class JumpPointSearch {
  Grid grid;
  CostHeuristic heuristic;
  JumpPointSearch(Grid this.grid, CostHeuristic this.heuristic);

  /// What are the nodes adjacent to the given [node], ordered by cost?
  List<Node> adjacentNodes(Node node) {
    var result = grid.adjacentNodes(node);
    result.sort((x, y) => heuristic(x, grid.endNode) - heuristic(y, grid.endNode));
    return result;
  }

  /// What are the diagonals (directions) along which we should search?
  List<Direction> directions(Node node) {
    Direction nodeToDirection(Node n) => new Direction(n.row - node.row, n.col - node.col);

    return adjacentNodes(node)
      .where((n) => n.row != node.row && n.col != node.col)
      .map(nodeToDirection).toList();
  }

  /// What is the next node in the given direction (null if the node is off the map or unwalkable)?
  Node nextNodeInDirection(Node startNode, Direction direction) {
    var newRow = startNode.row + direction.dy;
    var newCol = startNode.col + direction.dx;
    return (newRow >= grid.rows || newRow < 0 || newCol >= grid.columns || newCol < 0 || !grid.getNode(newRow, newCol).isWalkable) ?
      null :
      grid.getNode(newRow, newCol);
  }

  /// Is the end node in the given direction?
  bool inDirection(Node startNode, Direction direction) {
    var nextNode = nextNodeInDirection(startNode, direction);
    if(nextNode == null)
      return false;
    else if(nextNode == grid.endNode)
      return true;
    else
      return inDirection(nextNode, direction);
  }

  /// Is the end node in the given col, given a [startNode] and a column delta [dy]?
  bool inCol(Node startNode, int dy) {
    return inDirection(startNode, new Direction(dy, 0));
  }

  /// Is the end node in the given row, given a [startNode] and a row delta [dx]?
  bool inRow(Node startNode, int dx) {
    return inDirection(startNode, new Direction(0, dx));
  }

  /// What is the path of jump points from the start node to the end node?
  List<Node> get path {
    List<Node> recPath(List<Node> jumpPoints, Node startingNode, Direction direction) {
      if(startingNode == grid.endNode || inRow(startingNode, direction.dx) || inCol(startingNode, direction.dy)) {
        if(startingNode != jumpPoints.last && startingNode != grid.endNode)
          jumpPoints.add(startingNode);
        jumpPoints.add(grid.endNode);
        return jumpPoints;
      }
      var nextNode = nextNodeInDirection(startingNode, direction);
      if(nextNode == null)
        return null;
      else
        return recPath(jumpPoints, nextNode, direction);
    }
    var bestDirections = directions(grid.startNode);
    return recPath([grid.startNode], grid.startNode, bestDirections.first);
  }
}
