part of game_foundation;

class Node {
  int row, col;
  num f, g, h;
  bool isWalkable;
  Node parent;

  int operator ==(Node other) {
    return row == other.row && col == other.col;
  }

  Node(this.row, this.col);

  @override String toString() => "r:$row/c:$col";
}

