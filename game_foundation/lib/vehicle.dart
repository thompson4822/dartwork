part of game_foundation;

class Vehicle extends Sprite {
  static const String WRAP = "wrap";
  static const String BOUNCE = "bounce";

  Vector _position;

  Vector velocity;
  String edgeBehavior = WRAP;
  double mass = 1.0;
  num maxSpeed = 10;

  Vehicle() : _position = new Vector(0, 0), velocity = new Vector(0, 0) {
    draw();
    onEnterFrame.listen((_) => _update);
  }

  /**
   * Ensures the length of the vector is no longer than the given value.
   *
   * If the length is larger than [max], it will be truncated.
   */
  Vector truncate(Vector vector, num max) {
    var newLength = Math.min(max, vector.length);
    return new Vector.polar(newLength, vector.degrees);
  }

  void draw() {
    graphics
      ..clear()
      ..strokeColor(Color.Black, 1) // graphics.lineStyle(0) in AS3
      ..moveTo(10, 0)
      ..lineTo(-10, 5)
      ..lineTo(-10, -5)
      ..lineTo(10, 0);
  }

  void _update() {
    // Make sure the velocity stays within max speed
    velocity = truncate(velocity, maxSpeed);

    // Add velocity to position
    _position = _position + velocity;

    // Handle any edge behavior
    (edgeBehavior == WRAP) ? _wrap() : _bounce();

    // Update position of sprite
    x = position.x;
    y = position.y;

    // Rotate heading to match velocity
    // convert radians to degrees: Degrees = radians * 180 / PI
    rotation = velocity.degrees;
  }

  void _bounce() {
    if(stage != null) {
      var newX = Math.min(Math.max(0, position.x), stage.stageWidth);
      var newY = Math.min(Math.max(0,  position.y), stage.stageHeight);
      var vX = (position.x != newX) ? velocity.x * -1 : velocity.x;
      var vY = (position.y != newY) ? velocity.y * -1 : velocity.y;
      if(position.x != newX || position.y != newY) {
        position = new Vector(newX, newY);
        velocity = new Vector(vX, vY);
      }
    }
  }

  void _wrap(){
    if(stage != null) {
      var newX = position.x % stage.stageWidth;
      var newY = position.y % stage.stageHeight;
      if(position.x != newX || position.y != newY) {
        position = new Vector(newX, newY);
      }
    }
  }

  Vector get position => _position;

  void set position(Vector vector) {
    _position = vector;
    x = _position.x;
    y = _position.y;
  }

  @override set x(num value) {
    super.x = value;
    // Only create new vector if necessary (optimization)
    if(value != _position.x) _position = new Vector(value, _position.y);
  }

  @override set y(num value) {
    super.y = value;
    // Only create new vector if necessary (optimization)
    if(value != _position.y) _position = new Vector(_position.x, value);
  }
}