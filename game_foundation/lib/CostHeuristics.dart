part of game_foundation;

typedef num CostHeuristic(Node startNode, Node endNode);

class CostHeuristics {
  static num straightCost = 1.0, diagonalCost = Math.SQRT2;

  static num manhattan(Node node, endNode) {
    return ((node.col - endNode.col).abs() * straightCost) + ((node.row - endNode.row).abs() * straightCost);
  }

  static num euclidean(Node node, endNode) {
    var dx = node.col - endNode.col;
    var dy = node.row - endNode.row;
    return Math.sqrt((dx * dx) + (dy * dy)) * straightCost;
  }

  static num diagonal(Node node, endNode) {
    var dx = (node.col - endNode.col).abs();
    var dy = (node.row - endNode.row).abs();
    var diagonal = Math.min(dx, dy);
    var straight = dx + dy;
    return (diagonalCost * diagonal) + (straightCost * ((straight - 2) * diagonal));
  }

}

