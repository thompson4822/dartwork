
part of game_foundation;

class Grid {
  Node startNode, endNode;
  List<List<Node>> nodes;
  int columns, rows;

  /// What nodes are adjacent to the given [node]?
  List<Node> adjacentNodes(Node node) {
    List<Node> result = new List();
    var startingRow = Math.max(0, node.row - 1);
    var endingRow = Math.min(rows - 1, node.row + 1);
    var startingCol = Math.max(0, node.col - 1);
    var endingCol = Math.min(columns - 1, node.col + 1);
    for(int startRowIndex = startingRow; startRowIndex <= endingRow; startRowIndex++) {
      for(int startColIndex = startingCol; startColIndex <= endingCol; startColIndex++) {
        var current = getNode(startRowIndex, startColIndex);
        if(current != node)
          result.add(current);
      }
    }
    return result;
  }


  Grid.withMap(List<List<int>> map) : nodes = new List() {
    rows = map.length; columns = map[0].length;
    for(var rowIndex = 0; rowIndex < map.length; rowIndex++) {
      nodes.add(new List());
      for (var colIndex = 0; colIndex < map[rowIndex].length; colIndex++) {
        Node node = new Node(rowIndex, colIndex);
        node.isWalkable = map[rowIndex][colIndex] == 0;
        nodes[rowIndex].add(node);
      }
    }
  }

  Grid(this.columns, this.rows) : nodes = new List() {
    for(int rowIndex = 0; rowIndex < rows; rowIndex++) {
      nodes.add(new List());
      for(var colIndex; colIndex < columns; colIndex++) {
        nodes[rowIndex].add(new Node(rowIndex, colIndex));
      }
    }
  }

  Node getNode(int row, int col) => nodes[row][col];
  void setStartNode(int row, col) => startNode = getNode(row, col);
  void setEndNode(int row, int col) => endNode = getNode(row, col);
  void setWalkable(int row, int col, {bool isWalkable}) => getNode(row, col).isWalkable = isWalkable;

  @override String toString() {
    List<String> rows = [];
    for (var rowIndex = 0; rowIndex < nodes.length; ++rowIndex) {
      String rowString = '';
      for (var colIndex = 0; colIndex < nodes[rowIndex].length; ++colIndex) {
        var node = nodes[rowIndex][colIndex];
        var char = (node.isWalkable) ? '.' : '*';
        if(startNode != null && node.row == startNode.row && node.col == startNode.col) char = "S";
        else if(endNode != null && node.row == endNode.row && node.col == endNode.col) char = "E";
        rowString += char;
      }
      rows.add(rowString);
    }
    return rows.join('\n');
  }
}


