
import 'package:game_foundation/game_foundation.dart';

void main() {
  var grid = new Grid.withMap([
      [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  ]);

  grid.setStartNode(3, 1);
  grid.setEndNode(3, 9);
  print(grid.toString());

  //var vector = new Vector(1, 3);
}