import 'dart:html';
import 'dart:convert';
import 'package:intl/intl.dart';
import 'package:log_viewer/tree.dart';
import 'package:log_viewer/time_utils.dart';
import 'package:log_viewer/math_utils.dart';


/**
 * A nice general purpose exception for code that hasn't been implemented yet.
 */
class NotImplementedException implements Exception {
  NotImplementedException();
}

void main() {
  
  var logViewer = new LogViewer();
}

typedef bool FilterFunc(String text);

/**
 * A search filter. When given a search expression, can determine whether text matches.
 */
class SearchFilter {

  String expression;

  List<FilterFunc> filters = [];

  /// Construct a new search filter from the given [expression]
  SearchFilter(String this.expression) {
    _parseFilters();
  }

  /// Create all of the filters that are representative of the expression
  void _parseFilters() {
    // This assumes that there really isn't anything to parse - the expression is a search string.
    // We can do something more involved in the future.
    filters.add((String line) => line.contains(expression));
  }

  /// Does the given [text] satisfy the search criteria?
  bool matches(String text) {
    filters.where((filter) => filter(text)).length > 0;
  }
}

/**
 * A class that represents the information pertinent to a session.
 * 
 * Also supports a number of utility methods for dealing with strings that might contain session and time information
 */
class Session implements Comparable<Session> {
  final String sessionId;
  
  /// What is the time the session started?
  String get startTime => _timeFor(lines.first);
  
  /// What is the time the session finished?
  String get endTime =>
    _timeFor(lines.reversed.firstWhere((line) => _timeFor(line) != null));
  
  /// What happened during the session?
  List<String> lines = [];
  
  /// Add more detail to the session
  void add(String line) => lines.add(_clean(line));
  
  int compareTo(Session other) => startTime.compareTo(other.startTime);
  
  /// Create a new session with the given [sessionId] and [startTime]
  Session(this.sessionId);  
  
  static final Pattern _timePattern = new RegExp(r"^(\d{2}:\d{2}:\d{2})");
  static final Pattern _sessionPattern = new RegExp(r"sessionid=([a-z0-9]+)-([a-z0-9]+)-([a-z0-9]+)-([a-z0-9]+)-([a-z0-9]+)"); 
  
  @override String toString() {
    var timeStatistics = '(Started at $startTime, Time: $_elapsedTime, Length: ${lines.length})';
    var result = 'Session $sessionId';
    while(result.length < 20) 
      result = result + '.';
    return  '$result $timeStatistics';
  }

  /// How many lines of content are there for this session?
  int get length => lines.length;

  /// What is the tree representation of this session?
  TreeNode get tree =>
    new Branch(new Leaf(this.toString()), lines.map((line) => new Leaf(line)).toList());
  
  /// Create a session based on the given [line]
  factory Session.sessionFor(String line) {
    Session result;
    var sessionMatch = _sessionPattern.allMatches(line);
    if(sessionMatch.length > 0) {
      var matched = sessionMatch.first.group(5);
      result = new Session(matched);
    }
    return result;
  }
  
  /// What time did the given detail happen at?
  String _timeFor(String line) {
    var timeMatcher = _timePattern.allMatches(line);
    return (timeMatcher.length > 0) ? timeMatcher.first.group(0) : null;
  }

  /// How much time elapsed in this session?
  String get _elapsedTime {
    var start = new Time.parse(startTime);
    var end = new Time.parse(endTime);
    var delta = new Time.parse((end - start).toString());
    return TimePrinter.print(delta); 
  }
  
  /// What is the text for the given [line] if the session information is stripped out?
  String _clean(String line) {
    var matches = _sessionPattern.allMatches(line);
    String revisedText = line;
    if(matches.length > 0) {
      revisedText = line.split(_sessionPattern).join("");        
    }
    return revisedText;
  }
}

class LogViewer {
  /// Page filter elements
  CheckboxInputElement audit = querySelector('#audit');
  CheckboxInputElement kili = querySelector('#kili');
  CheckboxInputElement robot = querySelector('#robot');
  CheckboxInputElement showErrors = querySelector('#errors');
  CheckboxInputElement showWarnings = querySelector('#warnings');
  CheckboxInputElement showEvents = querySelector('#events');
  CheckboxInputElement showUnclassified = querySelector('#unclassified');
  DateInputElement startDateElement = querySelector('#startDate');
  DateInputElement endDateElement = querySelector('#endDate');

  /// The selector for the update button
  ButtonElement refresh = querySelector('#refresh');

  /// Determines whether refresh is enabled or not
  void setRefreshAvailability() {
    refresh.disabled = !(audit.checked || kili.checked || robot.checked);
  }
  
  /// The selector for the log, where we display all the actual information once fetched and filtered
  var logElement = querySelector('#log');
   
  /// Create the log handler and set up event listeners for the filters
  LogViewer() {
    // Don't fetch by default, as this can be an expensive operation.
    //fetchLog();
    audit.onChange.listen((_) => setRefreshAvailability());
    kili.onChange.listen((_) => setRefreshAvailability());
    robot.onChange.listen((_) => setRefreshAvailability());
    showErrors.onChange.listen((_) => querySelectorAll(".error").forEach((e){ e.hidden = !showErrors.checked; }) );
    showWarnings.onChange.listen((_) => querySelectorAll(".warning").forEach((e){ e.hidden = !showWarnings.checked; }) );
    showEvents.onChange.listen((_) => querySelectorAll(".event").forEach((e){ e.hidden = !showEvents.checked; }) );
    showUnclassified.onChange.listen((_) => querySelectorAll(".unclassified").forEach((e){ e.hidden = !showUnclassified.checked; }) );
    refresh.onClick.listen((_) => fetchLog());
  }

  /// Set log to empty
  void setLogEmpty() => querySelector("#log").text = "No Log Data";

  /// Grab the log information from the server
  void fetchLog() { 
    var req = new HttpRequest();
    var url = 'http://localhost:9000/log?$_logQueryString';
    refresh.disabled = true;
    print("Now fetching data from '$url'");
    HttpRequest.getString(url).then((response) {
      Map<String, List<String>> logInformation = JSON.decode(response);
      if(logInformation.length == 0) {
        setLogEmpty();
      } else {
        _parseResponse(logInformation);
        showLog();
      }
    })
    .catchError((_) => setLogEmpty())
    .whenComplete(() => refresh.disabled=false);
  }

  /// What query string will be needed to get the correct info from the server?
  String get _logQueryString {
    var formatter = new DateFormat('yyyy-MM-dd');
    
    var startDate = (startDateElement.value == '') ? new DateTime.now() : startDateElement.valueAsDate.toUtc();
    var endDate = (endDateElement.value == '') ? new DateTime.now() : endDateElement.valueAsDate.toUtc();
    return "startDate=${formatter.format(startDate)}"
      "&endDate=${formatter.format(endDate)}"
      "&includeAudit=${audit.checked}"
      "&includeKilimanjaro=${kili.checked}"
      "&includeRobot=${robot.checked}";
  }
  
  // Classifications
  static const ERROR = "error";
  static const WARN = "warning";
  static const EVENT = "event";
  static const UNCLASSIFIED = "unclassified";    

  /// What classes should be used for the given leaf?
  List<String> classesFor(Leaf leaf) {
    var selectorClasses = [];
    var text = leaf.toString();
    if(text.contains("ERROR", 0))
      selectorClasses.add(ERROR);
    if(text.contains("WARN", 0))
      selectorClasses.add(WARN);
    if(text.contains("DEBUG", 0))
      selectorClasses.add(EVENT);
    if(selectorClasses.isEmpty)
      selectorClasses = [UNCLASSIFIED];
    return selectorClasses;
  }

  /// What is the HTML5 version of the logs fetched from the server?
  List<TreeNode> logs;
  
  /// Turn the raw logs into the HTML5 logs
  void _parseResponse(Map<String, List<String>> logInformation) {
    var dates = logInformation.keys.toList();
    dates.sort(); 
    logs = dates.reversed.map((dateString) => dateNode(dateString, logInformation[dateString])).toList();
  }
 
  /// What is the tree node for the given [date] and its associated [logInformation]?
  TreeNode dateNode(String date, List<String> logInformation) {
    var sessions = parseSessions(logInformation);
    
    List<TreeNode> nodes = sessions.reversed.map((session) => session.tree).toList();
    
    List<int> sessionLengths = nodes.map((session) => (session is Branch) ? (session as Branch).nodes.length : 0).toList();
    
    var avgLinesPerSession = mean(sessionLengths).round(); 
    var dateString = '$date (Sessions: ${nodes.length}, Mean: ${avgLinesPerSession}, SD: ${stdDev(sessionLengths)})';
    return new Branch(new Leaf(dateString), nodes); 
  }

  /// What is the tree node for the given [session] and its associated [logInformation]?
  TreeNode sessionNode(Session session, List<String> logInformation) {
    var nodes = logInformation.map((i) => new Leaf(i)).toList();
    return new Branch(new Leaf('${session.toString()} ${nodes.length} lines'), nodes);  
  }
  
  /// What are the sessions and their associated log data for the given [lines]?
  List<Session> parseSessions(List<String> lines) {
    List<Session> result = [new Session("UNKNOWN")];
    lines.forEach((line) {
      var session = new Session.sessionFor(line);
      if(session != null && session.sessionId != result.last.sessionId)
        (result.last.length == 0) ? result = [session] : result.add(session);
      result.last.add(line);  
    });
    return result;
  }
  
  /// Show the log information in the browser
  void showLog() {
    logElement.children = logs.map((node) => new HtmlTreeBuilder(node, classesFor).html).toList();
  }
    
}

