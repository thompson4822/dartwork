library tree;

import 'dart:html';

/**
 * The abstract notion of a tree node, from which tree type structures can be built
 */
abstract class TreeNode {}

/**
 * A leaf is a tree node that is represented by its text
 * Note that this could be generalized in the future to be a generic class
 */
class Leaf implements TreeNode {
  String content;
  
  /// Create the leaf based on [content]
  Leaf(String this.content);
  
  @override String toString() => content.toString();
}

/**
 * A branch is a tree node that has both a leaf identifier as well as a list of subnodes
 */
class Branch implements TreeNode {
  Leaf leaf;
  List<TreeNode> nodes;

  /// Create a branch represented by a given [leaf] with the provided tree [nodes] as content
  Branch(Leaf this.leaf, List<TreeNode> this.nodes);
  
  @override String toString() => '$leaf : (${nodes.map((node) => node.toString()).join(', ')})';
}

/// Alias for a function that can be used to add CSS styles to a leaf element 
typedef List<String> LeafClassifierFunc(Leaf leaf);

/**
 * Given a tree, can build an HTML representation. Other builder representations (ie: console) 
 * are of course possible.
 */
class HtmlTreeBuilder {
  TreeNode tree;
  LeafClassifierFunc _classifier;
  
  LeafClassifierFunc get classifier {
    if(_classifier == null) {
      _classifier = (leaf) => [''];
    }
    return _classifier;
  }
  
  /// Create a builder for the given [tree] with a [_classifier] function
  HtmlTreeBuilder(TreeNode this.tree, LeafClassifierFunc this._classifier);
  
  /// What is the HTML representation of the tree?
  Element get html {
    var result;
    if(tree is Branch) {
      var branch = tree as Branch;
      result = new LIElement()
          ..children = [
              new InputElement(type: 'checkbox')
                ..id = '${branch.leaf}',
              new LabelElement()
                ..htmlFor = '${branch.leaf}'
                ..classes = []
                ..text = '${branch.leaf}',
              new UListElement()
                ..children = branch.nodes.map((n) => new HtmlTreeBuilder(n, classifier).html).toList()
            ];
        
    } else {
      var leaf = tree as Leaf;
      result = new LIElement()
        ..text = '$leaf'
        ..classes = classifier(leaf);
    }
      
    return result;
  }
}

