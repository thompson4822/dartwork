library math_utils;

import 'dart:math' as Math;

num mean(List<num> numbers) => 
  numbers.fold(0, (total, current) => total + current) / numbers.length;

num stdDev(List<num> numbers) {
  var m = mean(numbers);  
  var variance = numbers.fold(0, (total, current) => total + Math.pow(current - m, 2) ) / (numbers.length - 1);
  return Math.sqrt(variance);
}