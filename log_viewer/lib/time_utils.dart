library time_utils;

class Time {
  int hours, minutes, seconds, milliseconds;
  Time(int this.hours, {int this.minutes: 0, int this.seconds: 0, int this.milliseconds: 0});
  
  Time.parse(String timeString) {
    var componentPattern = new RegExp(r"^(\d+):(\d+):(\d+)(\.\d+)?$");
    var timeMatcher = componentPattern.allMatches(timeString);
    hours = int.parse(timeMatcher.first.group(1));
    minutes = int.parse(timeMatcher.first.group(2));
    seconds = int.parse(timeMatcher.first.group(3));
    milliseconds = (timeMatcher.first.group(4) != null) ? 
        int.parse(timeMatcher.first.group(4).substring(1)) : 0;
  }
  
  DateTime get relativeDateTime {
    var now = new DateTime.now();
    return new DateTime.utc(now.year, now.month, now.day, hours, minutes, seconds, milliseconds);    
  }
  
  Duration operator -(Time other) => this.relativeDateTime.difference(other.relativeDateTime);

}

class TimePrinter {
  static String asTwoDigitCode(int value) =>
    (value < 10) ? '0$value' : '$value';
  
  static String print(Time t) =>
    '${asTwoDigitCode(t.hours)}h, ${asTwoDigitCode(t.minutes)}m, ${asTwoDigitCode(t.seconds)}s';
}