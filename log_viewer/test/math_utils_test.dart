import 'package:unittest/unittest.dart';
import 'package:log_viewer/math_utils.dart';

void main() {
  test('Find mean', () {
    var m = mean([1, 2, 2, 3, 5]);
    expect(m, equals(2.6));
  });
  test('Standard deviation', () {
    var dev = stdDev([1, 2, 2, 3, 5]);
    expect(dev.toStringAsFixed(2), equals(1.52.toString()));
  });
  test('Standard deviation with 1 element', () {
    var dev = stdDev([1]);
    expect(dev.toStringAsFixed(2), equals('NaN'));
    
  });

}