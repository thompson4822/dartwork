import 'package:unittest/unittest.dart';
import 'package:log_viewer/time_utils.dart';

void main () {
  test('Time difference', () {
    var startTimeString = "15:00:40.694";
    var startTime = new Time.parse(startTimeString);
    var endTimeString = "18:11:25.479";
    var endTime = new Time.parse(endTimeString);
    Duration d = endTime - startTime;
    expect(d.toString(), equals('3:10:44.785000'));
  });

}