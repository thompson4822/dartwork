import 'package:unittest/unittest.dart';
import 'package:unittest/html_enhanced_config.dart';
import 'dart:math' as Math;
//import 'package:your_stuff/your_stuff.dart';

void main() {
  useHtmlEnhancedConfiguration();
  // NOTE - All of these tests are from Chapter 3 of 'Foundation ActionScript 3.0 Animation'
  group('Some basic trig tests to ensure that we understand the math and the functions', () {
    test('Sine test like the one in figure 3.8', () {
      // Sine is the ratio of the angle's opposite leg to the hypotenuse, once converted to radians
      var degrees = -30;
      // Since all of the trig functions take radians and not degrees, we have to turn degrees into radians
      var actual = Math.sin(degrees * (Math.PI / 180));
      var expected = -0.5;
      // Not a big fan of the following precision limiting technique!
      expect(double.parse(actual.toStringAsPrecision(2)), equals(expected));
      // So at this point we know that the length of the opposite side will be half as long as the length of the
      // hypotenuse. We also know that it will be in the negative y direction on the screen (north of the current location)
      // since the length of the hypotenuse is always positive.
    });
    test('Cosine test like the one in figure 3.10', (){
      // Cosine is the ratio of the angle's adjacent leg to the hypotenuse, once converted to radians
      var degrees = -30;
      // Since all of the trig functions take radians and not degrees, we have to turn degrees into radians
      var actual = Math.cos(degrees * (Math.PI / 180));
      var expected = 0.866;
      // Not a big fan of the following precision limiting technique!
      expect(double.parse(actual.toStringAsPrecision(3)), equals(expected));
    });
    test('Tangent test like the one in figure 3.12', (){
      // Tangent is the ratio of the angle's opposite leg to the angle's adjacent leg.
      var degrees = -30;
      // Since all of the trig functions take radians and not degrees, we have to turn degrees into radians
      var actual = Math.tan(degrees * (Math.PI / 180));
      var expected = -0.577;
      expect(double.parse(actual.toStringAsPrecision(3)), equals(expected));
    });
    test("Arcsine test, which I've skipped for now", (){
      expect(true, equals(true));
    });
    test('Arccosine, which I will skip for now', (){
      expect(true, equals(true));
    });
    test('Arctangent2, with which we can do awesome rotations!', (){
      var actual = Math.atan2(-1, -2) * 180 / Math.PI;
      var expected = -150.0;
      expect(double.parse(actual.toStringAsPrecision(2)), equals(expected));

    });

  });

}            