part of foundation_animation;

class Arrow extends Sprite {
  Arrow() {
    init();
  }

  void init() {
    graphics
      ..beginPath()
      ..moveTo(-50, -25)
      ..lineTo(0, -25)
      ..lineTo(0, -50)
      ..lineTo(50, 0)
      ..lineTo(0, 50)
      ..lineTo(0, 25)
      ..lineTo(-50, 25)
      ..lineTo(-50, -25)
      ..closePath()
      ..strokeColor(Color.Black, 1);
  }
}
