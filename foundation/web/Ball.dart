part of foundation_animation;

class Ball extends Sprite {
  num radius;
  int color;

  Ball({this.radius: 40, this.color: Color.Blue}) {
    init();
  }

  void init() {
    graphics
      ..beginPath()
      ..circle(0, 0, radius)
      ..closePath()
      ..fillColor(0xFF8ED6FF) // This is actually RGBA (1 byte for each, so 4 bytes) with the first byte alpha
      ..strokeColor(color, 5);
  }
}
