part of foundation_animation;

class Wave2 extends Sprite {
  double angle = 0.0;
  int centerY = 200;
  int range = 50;
  double xSpeed = 1.0;
  double ySpeed = 0.05;
  num xPos, yPos;
  //Stage stage;

  Wave2() { init(); }

  void init() {
    xPos = 0;
    graphics
      ..strokeColor(Color.Black, 1)
      ..moveTo(0, centerY);
    onEnterFrame.listen(enterFrameHandler);
  }

  void enterFrameHandler(Event event) {
    xPos += xSpeed;
    angle += ySpeed;
    yPos = centerY + Math.sin(angle) * range;
    graphics.lineTo(xPos, yPos);
  }

}

