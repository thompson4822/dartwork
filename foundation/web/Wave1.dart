part of foundation_animation;

class Wave1 extends DisplayObjectContainer {
  Ball ball;
  double angle = 0.0;
  int centerY = 200;
  int range = 50;
  double xSpeed = 1.0;
  double ySpeed = 0.05;
  Stage stage;

  Wave1(Stage this.stage) {
    init();
  }

  void init() {
    ball = new Ball()
      ..x = 0
      ..addTo(this);
    onEnterFrame.listen(enterFrameHandler);
  }

  void enterFrameHandler(Event event) {
    ball.x += xSpeed;
    // Note that there is a stage.width. Don't use this, but rather stage.stageWidth as below.
    if(ball.x > stage.stageWidth + ball.width)
      ball.x = 0;
    ball.y = centerY + Math.sin(angle) * range;
    angle += ySpeed;
  }

}

