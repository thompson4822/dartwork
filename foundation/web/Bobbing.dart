part of foundation_animation;

class Bobbing extends DisplayObjectContainer {
  Ball ball;
  double angle = 0.0;
  double speed = 0.1;
  Stage stage;

  Bobbing(Stage this.stage) {
    init();
  }

  void init() {
    ball = new Ball()
      ..x = stage.stageWidth / 2
      ..addTo(this);
    onEnterFrame.listen(enterFrameHandler);
  }

  void enterFrameHandler(Event event) {
    ball.y = stage.stageHeight / 2 + Math.sin(angle) * 50;
    angle += speed;
  }
}

