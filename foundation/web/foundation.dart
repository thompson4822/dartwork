library foundation_animation;

import 'dart:html' as html;
import 'dart:math' as Math;
import 'dart:async';
import 'dart:convert';
import 'package:stagexl/stagexl.dart';

part 'Arrow.dart';
part 'Ball.dart';
part 'RotateToMouse.dart';
part 'Bobbing.dart';
part 'Wave1.dart';
part 'Wave2.dart';
part 'Pulse.dart';
part 'Random.dart';

/// General purpose method of getting the canvas, stage and render loop ready.
Stage createStage() {
  // setup the Stage and RenderLoop
  var canvas = html.querySelector('#stage');
  return new Stage(canvas);
}

void main() {
  var stage = createStage();
  var renderLoop = new RenderLoop()
    ..addStage(stage);

  // Here is rotate to mouse, which draws an arrow which tracks the mouse position. Cool use of atan2
/*
  var rotateToMouse = new RotateToMouse(stage)
    ..addTo(stage);
*/

  // Here we are just using sine to bounce a ball up and down.
/*
  var bobbing = new Bobbing(stage)
    ..addTo(stage);
*/

  // Move a ball in a classic sine wave pattern.
/*
  var wave = new Wave1(stage)
    ..addTo(stage);
*/

  // Have ball grow and shrink as a side effect of the sine function.
/*
  var pulse = new Pulse(stage)
    ..addTo(stage);
*/

  // Move ball using two controlling angles and speeds, again using the sine function to make it all look sort of random.
/*
  var random = new Random(stage)
    ..addTo(stage);
*/

  // Instead of using the ball, draw an actual sine wave using the drawing API

  var container = new DefaultContainer(stage)
    ..addChild(new Wave2());




}

class DefaultContainer extends DisplayObjectContainer {
  Stage stage;
  DefaultContainer(Stage this.stage);
}