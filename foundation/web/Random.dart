part of foundation_animation;

class Random extends DisplayObjectContainer {

  Ball ball;
  Stage stage;
  double angleX = 0.0,
    angleY = 0.0,
    xSpeed = 0.07,
    ySpeed = 0.11;
  int centerX = 200,
    centerY = 200,
    range = 50;

  Random(Stage this.stage) { init(); }

  void init() {
    ball = new Ball()
      ..x = 0
      ..addTo(this);
    onEnterFrame.listen(enterFrameHandler);
  }

  void enterFrameHandler(Event event) {
    ball
      ..x = centerX + Math.sin(angleX) * range
      ..y = centerY + Math.sin(angleY) * range;
    angleX += xSpeed;
    angleY += ySpeed;
  }


}
