part of foundation_animation;

class RotateToMouse extends DisplayObjectContainer {
  Arrow arrow;
  Stage stage;

  RotateToMouse(Stage this.stage) {
    init();
  }

  void init() {
    arrow = new Arrow();
    addChild(arrow);
    arrow.x = stage.stageWidth / 2;
    arrow.y = stage.stageHeight / 2;
    onEnterFrame.listen(enterFrameHandler);
  }

  void enterFrameHandler(Event e) {
    var dx = mouseX - arrow.x;
    var dy = mouseY - arrow.y;
    // Note - rotation is in radians. Converting to degrees causes great suckage.
    arrow.rotation = Math.atan2(dy, dx);
  }
}

