part of foundation_animation;

class Pulse extends DisplayObjectContainer {
  Ball ball;
  double angle = 0.0;
  int centerScale = 1;
  double range = 0.5, speed = 0.1;
  Stage stage;

  Pulse(Stage this.stage) { init(); }

  void init() {
    ball = new Ball()
      ..x = stage.stageWidth / 2
      ..y = stage.stageHeight / 2
      ..addTo(this);
    onEnterFrame.listen(enterFrameHandler);
  }

  void enterFrameHandler(Event event) {
    ball.scaleX = ball.scaleY = centerScale + Math.sin(angle) * range;
    angle += speed;
  }

}

