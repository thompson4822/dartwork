part of game_lib;

/**
 * Create a puzzle manager that tracks not only a generated solution, but also the current state of the puzzle
 * as the player understands it.
 */
class PuzzleManager {
  /// The actual Sudoku puzzle solution which the player is trying to get
  Puzzle solution;

  /// The puzzle as the player understands it. When the puzzle and the solution match, the player has won.
  Puzzle puzzle;

  /// Handy random number generator.
  Random rand;

  /// How difficult is the game.
  int difficulty;

  StreamController<Location> _onReject;
  StreamController<Puzzle> _onGenerate;
  StreamController _onPuzzleSolved;

  /// Stream to indicate when a player guess has been rejected.
  Stream<Location> get onReject => _onReject.stream;

  /// Stream to indicate when a puzzle has been generated.
  Stream<Puzzle> get onGenerate => _onGenerate.stream;

  /// Stream to indicate when the puzzle has been solved.
  Stream get onPuzzleSolved => _onPuzzleSolved.stream;

  /// Create a new puzzle manager with the given difficulty.
  PuzzleManager(int this.difficulty) {
    rand = new Random(new DateTime.now().millisecondsSinceEpoch);
    _onReject = new StreamController.broadcast();
    _onGenerate = new StreamController.broadcast();
    _onPuzzleSolved = new StreamController.broadcast();
    solution = new Puzzle();
  }

  /// Create a solution and populate the puzzle for the player.
  void generate() {
    for(int index = 0; index < solution.puzzle.length; index++) {
      solution.puzzle[index] = 0;
    }
    solution = newSolution();
    puzzle = createPuzzle(solution);
    _onGenerate.add(solution);
  }

  /// Given a [solution] what is a puzzle that would match the difficulty level
  Puzzle initialPuzzle(Puzzle solution) {
    var result = new Puzzle.clone(solution);
    while(result.emptyCellCount != difficulty) {
      result.puzzle[rand.nextInt(81)] = 0;
    }
    return result;
  }

  /// What is a puzzle for the player based off the given [solution]?
  Puzzle createPuzzle(Puzzle solution) {
    // Find a puzzle that has a unique solution
    var initial = initialPuzzle(solution);
    var clues = 5;
    var unsolvedIndices = new List();
    while((unsolvedIndices = solveNakedSingles(initial).unsolvedIndices).length > 0 && clues-- > 0) {
      var cellToFill = unsolvedIndices[rand.nextInt(unsolvedIndices.length)];
      initial.puzzle[cellToFill] = solution.puzzle[cellToFill];
    }
    return (unsolvedIndices.length == 0) ? initial : createPuzzle(solution);
  }

  /// What is a randomly generated and valid Sudoku solution?
  Puzzle newSolution() {
    var result = new Puzzle();
    bool genSolution(Puzzle solution, int index) {
      if(index == solution.puzzle.length) {
        return true;
      }
      else {
        var solved = false;
        var possible = solution.possibleValues((index / 9).floor(), (index % 9));
        while(possible.length > 0 && solved == false) {
          solution.puzzle[index] = possible[rand.nextInt(possible.length)];
          possible.remove(solution.puzzle[index]);
          solved = genSolution(solution, index + 1);
        }
        if(!solved)
          solution.puzzle[index] = 0;
        return solved;
      }
    }
    genSolution(result, 0);
    return result;
  }

  /// Recursively solve naked singles in a puzzle
  Puzzle solveNakedSingles(Puzzle p) {
    Puzzle solveNakedRec(Puzzle puzzle) {
      var found = 0;
      for(int row = 0; row < 9; row++) {
        for(int col = 0; col < 9; col++) {
          if(puzzle.gridAt(row, col) == 0) {
            var possibilities = puzzle.possibleValues(row, col);
            if(possibilities.length == 1) {
              puzzle.setValue(possibilities[0], row, col);
              found++;
            }
          }
        }
      }
      return (found > 0) ? solveNakedSingles(puzzle) : puzzle;
    }
    return solveNakedRec(new Puzzle.clone(p));
  }

  /// Is the given [value] valid for the [row] and [col]?
  bool validValue(int value, int row, int col) {
    return puzzle.possibleValues(row, col).contains(value);
  }

  bool get isSolved {
    var result = true;
    for(int rowIndex = 0; rowIndex < Puzzle.rows; rowIndex++) {
      for(int colIndex = 0; colIndex < Puzzle.cols && result == true; colIndex++) {
        result = solution.gridAt(rowIndex, colIndex) == puzzle.gridAt(rowIndex, colIndex);
      }
    }
    return result;
  }

  /// Set the [value] for the given [row] and [col]
  void setValue(int value, int row, int col) {
    var possible = puzzle.possibleValues(row, col);
    puzzle.setValue(value, row, col);
    if(possible.contains(value) || value == 0) {
      if(isSolved)
        _onPuzzleSolved.add(true);
    }
    else {
      _onReject.add(new Location(row, col));
    }
  }
}
