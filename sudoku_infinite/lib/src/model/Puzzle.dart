part of game_lib;

/**
 * Creates a new puzzle
 */
class Puzzle {
  List<int> puzzle;

  /// Create a new puzzle with an optional initial puzzle
  Puzzle({List<int> initial}) {
    if(initial != null)
      puzzle = new List.from(initial, growable: false);
    else
      puzzle = new List.filled(81, 0);
  }

  /// Produce a clone of the given puzzle
  Puzzle.clone(Puzzle other) : this(initial: other.puzzle);

  /// What are the values for the given [row] of the puzzle?
  List<int> rowValues(int row) {
    List<int> result = [];
    for(int index = 0; index < 9; index++)
      result.add(gridAt(row, index));
    return result;
  }

  /// What are the values for the given [column] of the puzzle?
  List<int> columnValues(int column) {
    List<int> result = [];
    for(int row = 0; row < 9; row++)
      result.add(gridAt(row, column));
    return result;
  }

  /// What value is at the given [row] and [col]?
  int gridAt(int row, int col) => puzzle[row * 9 + col];

  /// What are the values for the unit in which the given [row] and [column] exist?
  List<int> unitValues(int row, int column) {
    List<int> result = [];
    var rowStart = (row / 3).floor() * 3;
    var colStart = (column / 3).floor() * 3;
    for(int rowIndex = rowStart; rowIndex < rowStart + 3; rowIndex++) {
      for(int colIndex = colStart; colIndex < colStart + 3; colIndex++) {
        result.add(gridAt(rowIndex, colIndex));
      }
    }
    return result;
  }

  /// Set the given [row] and [column] to [value]
  void setValue(int value, int row, int column) {
    puzzle[row * 9 + column] = value;
    // Needs to create an event if the value is not valid. The event should indicate 
    // the conflict
  }

  /// What are the possible values that could be placed at the given location?
  List<int> possibleValues(int row, int column) {
    List<int> result = [];
    var used = rowValues(row)
      ..addAll(columnValues(column))
      ..addAll(unitValues(row, column));
    for(int candidate = 1; candidate <= 9; candidate++) {
      if(used.contains(candidate) == false)
        result.add(candidate);
    }
    return result;
  }

  /// How many cells in the puzzle are empty?
  int get emptyCellCount => puzzle.where((x) => x == 0).length;

  String toString() {
    String gridToString(List<int> grid) {
      var result = "";
      for(int row = 0; row < 9; row++) {
        var i = row * 9;
        if(row % 3 == 0 && row > 0)
          result += '---------------------\n';
        result += "${grid[i]} ${grid[i+1]} ${grid[i+2]} | ${grid[i+3]} ${grid[i+4]} ${grid[i+5]} | ${grid[i+6]} ${grid[i+7]} ${grid[i+8]}\n";
      }
      return result;
    }
    return "${gridToString(puzzle)}";
  }

  /// Is the puzzle solved (has no more empty cells)?
  bool get isSolved => puzzle.contains(0) == false;

  /// What indices represent the empty cells in the puzzle?
  List<int> get unsolvedIndices {
    var result = new List();
    for(int index = 0; index < puzzle.length; index++)
      if(puzzle[index] == 0)
        result.add(index);
    return result;
  }

  static int get rows => 9;

  static int get cols => 9;
}

