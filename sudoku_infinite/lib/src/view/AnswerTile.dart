part of game_lib;

/**
 * Class that represents an immutable answer tile which is a clue for the player. Unlike other tiles,
 * its value cannot be altered by the player.
 */
class AnswerTile extends Tile {

  /// Create a new tile with the given manager and value
  AnswerTile(TileManager tileManager, int value) : super(tileManager) {
    state = TileManager.INITIAL;
    this.value = value;
  }

  /// Set the state for the tile. Note that this must always be the initial state because of the
  /// immutability of the tile.
  @override
  void set state(String state) {
    if(numChildren > 0)
      removeChildAt(0);
    addChildAt(tileManager.forState(TileManager.INITIAL), 0);
  }

}

