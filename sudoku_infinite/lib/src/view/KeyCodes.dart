part of game_lib;

/**
 * This class works around my lack of knowledge of StageXL. There is probably a more complete class that is part of
 * the library - more research is needed here.
 */
class KeyCodes {
  // TODO - A partial list of useful codes. Expand in the future as necessary
  static final int BACKSPACE = 8;
  static final int TAB = 9;
  static final int ENTER = 13;
  static final int SHIFT = 16;
  static final int CONTROL = 17;
  static final int CAPS_LOCK = 20;
  static final int ESC = 27;
  static final int SPACEBAR = 32;
  static final int PAGE_UP = 33;
  static final int PAGE_DOWN = 34;
  static final int END = 35;
  static final int HOME = 36;
  static final int LEFT_ARROW = 37;
  static final int UP_ARROW = 38;
  static final int RIGHT_ARROW = 39;
  static final int DOWN_ARROW = 40;
  static final int INSERT = 45;
  static final int DELETE = 46;
}

