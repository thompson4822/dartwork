part of game_lib;

/**
 * Class to represent the special empty tile - that is a tile that the user has not figured out the
 * answer to.
 */
class EmptyTile extends Tile {

  /// Create a new tile using the given tile manager
  EmptyTile(TileManager manager) : super(manager) {
    value = 0;
    state = TileManager.EMPTY;
    onMouseOver.listen((_) {
      // If you don't do this, you will not be able to 'hear' keyboard events
      stage.focus = this;
      state = TileManager.CURSOR;
    });
    onMouseOut.listen((_){
      state = TileManager.EMPTY;
    });
  }
}

