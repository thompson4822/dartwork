part of game_lib;

/**
 * A class that represents the classic 9x9 Sudoku grid. It is responsible for managing its tiles (individual
 * Sudoku cells)
 */
class Grid extends DisplayObjectContainer {
  ResourceManager resources;

  List<Tile> tiles = new List();

  /// What is the model for the Sudoku puzzle?
  PuzzleManager currentPuzzle = new PuzzleManager(SudokuDifficulty.EASY);

  TileManager _tileManager;

  /// What is the tile manager that we'll use for the individual tiles?
  TileManager get tileManager {
    if(_tileManager == null)
      _tileManager = new TileManager(resources.getTextureAtlas('game_tiles'));
    return _tileManager;
  }

  /// What is a tile that could represent the cell for the given [row] and [col] of the puzzle?
  Tile newTile(int row, int col) {
    var tile;
    var puzzleValue = currentPuzzle.puzzle.gridAt(row, col);
    if( puzzleValue != 0)
      tile = new AnswerTile(tileManager, puzzleValue);
    else {
      tile = new EmptyTile(tileManager);
      tile
        ..onKeyDown.listen((KeyboardEvent e) => keyPressHandler(tile, e.keyCode, row, col))
        ..onMouseClick.listen((_) => mouseClickHandler(tile, row, col));
    }
    // TODO - The following offsets should be calculated and not hard coded
    tile
      ..x = col * 200 + ((col / 3).floor() * 40)
      ..y = row * 200 + ((row / 3).floor() * 40)
      ..onMouseOver.listen((_) => highlight(row, col))
      ..onMouseOut.listen((_) => clearHighlighted())
      ..addTo(this);
    return tile;
  }

  Tile tileAt(int row, int col) =>
    tiles[row * Puzzle.cols + col];

  /// Handle any clicks on the given [tile] at the [row] and [col]
  void mouseClickHandler(Tile tile, int row, int col) {
    print(currentPuzzle.puzzle.possibleValues(row, col));
    tile.isValid = true;
    tile.value = 1;
    currentPuzzle.setValue(tile.value, row, col);
  }

  /// Handle any keyboard activity in a [tile] at [row] and [col] in the puzzle
  void keyPressHandler(Tile tile, int keyCode, int row, int col) {
    // The following jumps through some hoops to 
    // accomodate Dart's lack of something like itoa/atoi.
    // Seems like a good library opportunity
    var value = 0;
    tile.isValid = true;
    if(keyCode >= '0'.codeUnits[0] && keyCode <= '9'.codeUnits[0]) {
      print(currentPuzzle.puzzle.possibleValues(row, col));
      value = int.parse(UTF8.decode([keyCode]));
      tile.value = value;
      currentPuzzle.setValue(value, row, col);
    }
    else if(keyCode == KeyCodes.BACKSPACE || keyCode == KeyCodes.DELETE) {
      tile.value = value;
      currentPuzzle.setValue(value, row, col);
    }
  }

  void handleRejection(Location location) {
    tileAt(location.row, location.col).isValid = false;
  }

  /// Has the player won?
  bool get boardComplete {
    return tiles.every((tile) => tile.state != TileManager.WRONG && tile.value != 0);
  }

  /// Show the player the cruel winning message
  void showWin() {
    print('Dude, you totally Seahawked that Bronco!');
  }

  /// Make a distinctive mark between all 3x3 units.
  void drawUnitBoundaries() {
    var g = new Graphics()
      ..beginPath()
      ..lineTo(1000,1000)
      ..closePath()
      ..strokeColor(Color.Black, 10);

    var lines = new Sprite();
    lines.graphics
      ..beginPath()
    // Vertical lines
      ..moveTo(610, 0)
      ..lineTo(610,1850)
      ..moveTo(1250, 0)
      ..lineTo(1250,1850)

    // Horizontal lines
      ..moveTo(0, 610)
      ..lineTo(1850, 610)
      ..moveTo(0, 1250)
      ..lineTo(1850, 1250)

      ..closePath()
      ..strokeColor(Color.Black, 20);

    lines.addTo(this);
  }

  /// Create a grid with the given [resources]
  Grid(ResourceManager this.resources) {
    currentPuzzle.onGenerate.listen((_) => print("You bastard, you just created a new puzzle!"));
    currentPuzzle.onPuzzleSolved.listen((_) => showWin());
    currentPuzzle.onReject.listen(handleRejection);
    currentPuzzle.generate();
    drawUnitBoundaries();
    for(int row = 0; row < 9; row++) {
      for(int col = 0; col < 9; col++) {
        tiles.add(newTile(row, col));
      }
    }
  }

  /// What tiles are currently highlighted, as determined by the position of the cursor over the board?
  List<Tile> highlighted = new List();

  /// What are the tiles in the given [row]?
  List<Tile> tilesInRow(int row) {
    var initialIndex = row * 9;
    var sublist = tiles.sublist(initialIndex, initialIndex + 9);
    return sublist;
  }

  /// What are the tiles in the given [col]
  List<Tile> tilesInColumn(int col) {
    var result = new List();
    for(int row = 0; row < 9; row++) {
      result.add(tiles[row * 9 + col]);
    }
    return result;
  }

  /// What are the tiles in the current unit, in which the given [row] and [col] can be found.
  List<Tile> tilesInUnit(int row, int col) {
    var result = new List();
    var rowStart = (row / 3).floor() * 3;
    var colStart = (col / 3).floor() * 3;
    for(int rowIndex = rowStart; rowIndex < rowStart + 3; rowIndex++) {
      for(int colIndex = colStart; colIndex < colStart + 3; colIndex++) {
        result.add(tiles[rowIndex * 9 + colIndex]);
      }
    }
    return result;

  }

  /// What items should be highlighted, based on the cursor being over the tile in the given [row] and [col]?
  List<Tile> itemsToHighlight(int row, int col) {
    // Kind of a Dart kludge to get around the lack of a unique(T=>bool) in the collections
    var highlightItems = new Map<Tile, bool>();
    tilesInColumn(col).forEach((tile) => highlightItems[tile] = true);
    tilesInRow(row).forEach((tile) => highlightItems[tile] = true);
    tilesInUnit(row, col).forEach((tile) => highlightItems[tile] = true);
    return highlightItems.keys.toList();
  }

  /// When the cursor moves, make sure to clear any highlighting that might exist.
  void clearHighlighted() {
    if(highlighted != null)
      highlighted.forEach((tile) => tile.state = TileManager.EMPTY);
    highlighted = new List();
  }

  /// Highlight all tiles in the same unit, row, and col as the given tile.
  void highlight(int row, int col) {
    var currentTile = tiles[row * 9 + col];
    itemsToHighlight(row, col)
    .where((tile) => tile != currentTile)
    .forEach((tile){
      tile.state = TileManager.HIGHLIGHT;
      highlighted.add(tile);
    });
  }
}

