part of game_lib;

/**
 * Class that has a rough representation of difficulty, based incorrectly on how many cells the player has to
 * deduct the answer to.
 */
class SudokuDifficulty {
  static int EASY = 45;
  static int MEDIUM = 49;
  static int DIFFICULT = 53;
  static int NIGHTMARE = 58;
}

