library game_lib;

import 'dart:html' as html;
import 'dart:math';
import 'dart:async';
import 'dart:convert';
import 'package:stagexl/stagexl.dart';

part 'src/sudoku_difficulty.dart';

part 'src/view/AnswerTile.dart';
part 'src/view/EmptyTile.dart';
part 'src/view/Grid.dart';
part 'src/view/KeyCodes.dart';
part 'src/view/Tile.dart';
part 'src/view/TileManager.dart';

part 'src/model/Location.dart';
part 'src/model/Puzzle.dart';
part 'src/model/PuzzleManager.dart';

Stage stage = null;
RenderLoop renderLoop;

/// General purpose method of getting the canvas, stage and render loop ready.
void setupStage() {
  // setup the Stage and RenderLoop 
  var canvas = html.querySelector('#stage');
  canvas.focus();
  stage = new Stage(canvas);
  stage.align = StageAlign.TOP_LEFT;
  renderLoop = new RenderLoop();
  renderLoop.addStage(stage);
}

void setup() {
  setupStage();
  var resourceManager = new ResourceManager()
    ..addTextureAtlas("game_tiles", "images/game_tiles.json", TextureAtlasFormat.JSONARRAY);
  resourceManager.load().then((result) => startGame(resourceManager));
}

class SoundManager {
  
}

void startGame(ResourceManager resources) {
  var grid = new Grid(resources)
    ..addTo(stage);
}



