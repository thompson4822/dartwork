import 'package:unittest/unittest.dart';
import 'package:unittest/html_enhanced_config.dart';
import 'package:sudoku_infinite/game_lib.dart';

void main() {
  useHtmlEnhancedConfiguration();
  group('Basic Puzzle Queries', (){
    var puzzle = new Puzzle(initial: [
      1, 2, 3, 4, 5, 6, 7, 8, 9,
      4, 5, 6, 7, 8, 9, 1, 2, 3,
      7, 8, 9, 1, 2, 3, 4, 5, 6,
      2, 3, 4, 5, 6, 7, 8, 9, 1,
      5, 6, 7, 8, 9, 1, 2, 3, 4,
      8, 9, 1, 2, 3, 4, 5, 6, 7,
      3, 4, 5, 6, 7, 8, 9, 1, 2,
      6, 7, 8, 9, 1, 2, 3, 4, 5,
      9, 1, 2, 3, 4, 5, 6, 7, 8]);
    test('First Row', () => puzzle.rowValues(0) == [1, 2, 3, 4, 5, 6, 7, 8, 9]);
    test('Last Row', () => puzzle.rowValues(8) == [9, 1, 2, 3, 4, 5, 6, 7, 8]);
    test('First Column', () => puzzle.columnValues(0) == [1, 4, 7, 2, 5, 8, 3, 6, 9]);
    test('Last Column', () => puzzle.columnValues(8) == [9, 3, 6, 1, 4, 7, 2, 5, 8]);
    test('First Unit', () => puzzle.unitValues(0, 0) == [1, 2, 3, 4, 5, 6, 7, 8, 9]);
    test('Middle Unit', () => puzzle.unitValues(4, 4) == [5, 6, 7, 8, 9 , 1, 2, 3, 4]);
    test('Last Unit', () => puzzle.unitValues(8, 8) == [9, 1, 2, 3, 4, 5, 6, 7, 8]);
  });
  group('Possible Values', (){
    var puzzle = new Puzzle(initial: [
      1, 2, 3, 4, 5, 0, 0, 8, 9,
      4, 0, 6, 7, 8, 9, 1, 2, 3,
      7, 8, 9, 1, 2, 3, 4, 5, 6,
      2, 3, 4, 5, 6, 7, 8, 9, 1,
      0, 0, 0, 0, 0, 0, 0, 0, 0,
      8, 9, 1, 2, 3, 4, 5, 6, 7,
      3, 4, 5, 6, 7, 8, 0, 1, 2,
      6, 7, 8, 9, 1, 2, 3, 4, 5,
      0, 1, 2, 3, 4, 5, 6, 7, 8]);
    test('First Positional', () => puzzle.possibleValues(1, 1) == [5]);
    test('Second Positional', () => puzzle.possibleValues(4, 1) == [5, 6]);
    test('Third Positional', () => puzzle.possibleValues(4, 6) == [2, 7, 9]);
  });
  group('Valid Puzzle', (){
//    var puzzle = new SudokuModel(SudokuDifficulty.EASY);
//    var grid = puzzle.solution;
//    test('Cells have single solution', (){
//      var valid = true;
//      for(int index = 0; index < grid.length && valid == true; index++) {
//        var solutionValue = grid[index];
//        grid[index] = 0;
//        if(puzzle.possibleValues((index / 8).floor(), index % 8) != [solutionValue])
//          valid = false;
//      }
//      valid == true;
//    });  
  });
}